import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import OrderCard from './components/OrderCard';
import EditProduct from './components/EditProduct';
import AddProduct from './components/AddProduct';
import Dash from './components/Dash';
import Home from './pages/Home';
import Cart from './pages/Cart';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import './App.css';
import {UserProvider} from './UserContext';

function App() {

  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  //for clearing out the local storage upon logging out
  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{
    fetch(`https://capstone-2-comia-248.onrender.com/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{

      console.log(data);

      if(typeof data._id !=="undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[])

//wrap all the components within the UserProvider to allow rerendering when the value changes

  return (
      
      <UserProvider value={{user, setUser, unsetUser}}>
          <Router>
               <AppNavbar/>
               <Container>
                  <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="/products" element={<Products/>} />
                    <Route path="/products/:productId" element={<ProductView/>} />
                    <Route path="/cart" element={<Cart/>} />
                    <Route path="/cart/:userId" element={<OrderCard/>} />
                    <Route path="/addProduct" element={<AddProduct/>} />
                    <Route path="/editProduct/:productId" element={<EditProduct/>} />
                    <Route path="/admin" element={<Dash/>} />
                    <Route path="/login" element={<Login/>} />
                    <Route path="/logout" element={<Logout/>} />
                    <Route path="*" element={<Error/>}/> 
                    <Route path="/register" element={<Register/>} />
                   </Routes>
               </Container>
              </Router>
      </UserProvider>
  );
}

export default App;
