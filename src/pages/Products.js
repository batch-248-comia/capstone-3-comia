//import courses from '../data/courses';
import {useEffect, useState, useContext} from 'react';
import { Navigate } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import {Row} from 'react-bootstrap'


export default function Products(){
	
	//check to see if the mock data was captured
	//console.log(courses);
	// console.log(courses[0]);

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(()=>{

		fetch(`https://capstone-2-comia-248.onrender.com/products/all`)
		.then(res=>res.json())
		.then(data=>{

			const productArr = (data.map(product=>{


				return(

					<ProductCard productProp={product} key={product._id} breakpoint={4}/>
				)
			}))
			setProducts(productArr)
		})
	},[products])

	/*const coursesMap = courses.map(course=>{
		return(
			<CourseCard key={course.id} courseProp={course}/>
		)
	})*/


	return(

		(user.isAdmin)?
		<Navigate to="/admin"/>
		:
		<>
			<h1>Products</h1>
			<Row>
			{products}
			</Row>
		</>

	)


}