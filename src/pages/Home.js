import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';

const data = {
	title: "Learn Yard",
	content: "One-Stop Shop for School and Office Supplies.",
	destination: "/products",
	label: "Shop now!"
}


export default function Home(){

	return (
		<>
			<Banner data={data}/>
    		<Highlights/>
    		{/*<CourseCard/>*/}
		</>
	)
}