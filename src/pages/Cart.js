//import courses from '../data/courses';
import {useEffect, useState, useContext} from 'react';
import { Navigate } from 'react-router-dom';
import OrderCard from '../components/OrderCard';
import UserContext from '../UserContext';
import {Row} from 'react-bootstrap'


export default function Cart(){
	
	//check to see if the mock data was captured
	//console.log(courses);
	// console.log(courses[0]);

	const [orders, setOrders] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(()=>{

		fetch(`https://capstone-2-comia-248.onrender.com/cart/getAllOrders`)
		.then(res=>res.json())
		.then(data=>{

			const orderArr = (data.map(order=>{


				return(

					<OrderCard orderProp={order} key={order._id} breakpoint={4}/>
				)
			}))
			setOrders(orderArr)
		})
	},[orders])

	/*const coursesMap = courses.map(course=>{
		return(
			<CourseCard key={course.id} courseProp={course}/>
		)
	})*/


	return(

		(user.isAdmin)?
		<Navigate to="/admin"/>
		:
		<>
			<h1>Cart</h1>
			<Row>
			{orders}
			</Row>
		</>

	)


}