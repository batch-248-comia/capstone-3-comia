import { Link } from 'react-router-dom'
import {Row, Col, Card, Button} from 'react-bootstrap';


export default function ProductCard(props) {

 

  const {breakpoint, productProp } = props;
  
  const {name, description, price, _id, imageLink} = productProp;

  
  return (
      <Col xs={12} md={breakpoint} className="mt-4"> 
        <Card className="cardHighlight p-3">
        
        <Card.Img variant="top" fluid="true" src={`${imageLink}`} width="150" height="300"/>
          <Card.Body>
            <Card.Title as={Link} to={`/products/${_id}`}>{name}</Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>
              {description}
            </Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>
              Php {price}
            </Card.Text>
           {/*} <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>*/}
          </Card.Body>
        </Card>
      </Col>
  )
}