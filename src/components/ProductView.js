import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [imageLink, setImageLink] = useState("");
	const [quantity, setQuantity] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);

	const addOrder = (productId) =>{

		fetch(`https://capstone-2-comia-248.onrender.com/orders/cart/${user.id}`,{
			method:"POST",
			headers:{
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productId: productId, 
				quantity: quantity,
				totalAmount: totalAmount
				
			})
		})
		.then(res=>res.json())
		.then(data=>{
				console.log(data)

				if(data===true){
					Swal.fire({
						title:"Successfully ordered!",
						icon:"success",
						text:"Item ordered!"
					})

					navigate("/products")

				}else{
					Swal.fire({
						title:"Something went wrong!",
						icon:"error",
						text:"Please try again!"
					})
				}
			})
	}




	useEffect(()=>{
		console.log(user.id)
		console.log(productId)

		fetch(`https://capstone-2-comia-248.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setImageLink(data.imageLink)
			setQuantity(data.quantity)
		})

	},[])

	return (
	<Container className ="mt-5">
		    <Row >
		      <Col lg={{span:6, offset:3}}>
		          <Card className="cardHighlight p-3">
		          <Card.Img variant="top" fluid="true" src={`${imageLink}`} width="150" height="300"/>
		            <Card.Body>
		              <Card.Title> {name}</Card.Title>
		              <Card.Subtitle>Description</Card.Subtitle>
		              <Card.Text>
		                {description}
		              </Card.Text>
		              <Card.Subtitle>Price</Card.Subtitle>
		              <Card.Text>
		                Php {price}
		              </Card.Text>
		               <div>
				            <span>Quantity</span>
				            <button onClick={() => {
				            	(quantity > 1) ? 
				            	setQuantity(quantity - 1) 
				            	:
				            	setQuantity(0);
				              }}
				               >-</button>
				               {quantity}
				            <button onClick={() => setQuantity(quantity + 1)}>+</button>
				     				            
         				 </div>
		              {
		              	(user.id !==null)?
		              	<Button variant="primary" onClick={()=>addOrder(productId)}>Order</Button>
		              	:
		              	<Link className="btn btn-danger" to="/login">Login to Order</Link>

		              }

		              

		            </Card.Body>
		          </Card>
		      </Col>
		    </Row>
	</Container>
  )


}