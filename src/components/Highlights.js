import {Row, Col, Card} from 'react-bootstrap';


export default function Highlights(){

	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					<Card.Img variant="top" fluid="true" src={`https://www.verywellfamily.com/thmb/RgTVN9VG3mOhuM7yKIagDDdp9mI=/fit-in/1500x493/filters:no_upscale():max_bytes(150000):strip_icc()/81qgjxwiOsL._AC_SX679_-897802fe6aff46c083014aef49c17c37.jpg`} width="150" height="300"/>
				      <Card.Body>
				        <Card.Title>School Supplies in One Click!</Card.Title>
				        <Card.Text>
				          From backpack essentials to new-generation needs, we've rounded up school supplies for your kids to use.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
			<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					<Card.Img variant="top" fluid="true" src={`https://m.media-amazon.com/images/W/IMAGERENDERING_521856-T1/images/I/81oxUZimtuL._AC_SL1500_.jpg`} width="150" height="300"/>
				      <Card.Body>
				        <Card.Title>Office Supplies in a Rush!</Card.Title>
				        <Card.Text>
				           From papers, filing and storage supplies, ink and toners, presentation and display aids, we have them all in our online store. An excellent Procurement Partner for All of Your Office Supplies.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
			<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
					<Card.Img variant="top" fluid="true" src={`https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjtfUKK8g7_R4YKZOgfYNmzf4nuagP0Vrcmw&usqp=CAU`} width="150" height="300"/>
				      <Card.Body>
				        <Card.Title>Get Promos and Discounts!</Card.Title>
				        <Card.Text>
				          Purchase Stationery Items Online At Best Prices. Shop Safely From Home. High Quality. Affordable. Online Payment. Fast & Efficient Delivery. 
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
		</Row>
		)
}